package no.martinsolaas.jira.testapi;

import com.atlassian.jira.rest.client.api.JiraRestClient;
import com.atlassian.jira.rest.client.api.domain.Issue;
import com.atlassian.jira.rest.client.internal.async.AsynchronousJiraRestClientFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.net.URI;

@SpringBootApplication
public class TestapiApplication implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(TestapiApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        new MainClass().run();
    }

    static class MainClass {

        public void run() throws Exception {
            JiraRestClient client = getJiraRestClient();
            Issue issue = client.getIssueClient().getIssue("CF-124").claim();
            issue.getFields().forEach(f -> {
                System.out.println(f.getName());
            });
        }

        private JiraRestClient getJiraRestClient() throws java.net.URISyntaxException {
            return new AsynchronousJiraRestClientFactory()
                    .createWithBasicHttpAuthentication(URI.create("https://jira.xxx.yy")
                            , "jill.doe@anywhere.com"
                            , "in-your-dreams");
        }
    }
}

